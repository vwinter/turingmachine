structure Semantics = struct

  open Model; 
  open CONCRETE_REPRESENTATION;

  fun M(itree(inode("program", _), 
    [ 
      initialization,
      whileLoop,
      itree(inode("optionalNewlns", _), [])
    ]), m) =
      let
        (* Program Initializes with rule_fired = true so while loop is executed at least once *)
        val m1 = updateEnv("rule_fired", BOOL, m)
        val m2 = updateStore(getLoc(accessEnv("rule_fired", m1)), boolean "true", m1)
        val m3 = M(whileLoop, M(initialization, m2))
      in
        m3
      end
        
  | M(itree(inode("initialization", _),
    [
      tapeInitialization,
      startInitialization,
      rwHeadInitialization
    ]), m) = M(rwHeadInitialization, M(startInitialization, M(tapeInitialization, m)))

  | M(itree(inode("tapeInitialization", _),
    [
      t1, itree(inode("=", _), []), v1, itree(inode("newln", _), []),
      t2, itree(inode("=", _), []), v2, itree(inode("newln", _), []),
      t3, itree(inode("=", _), []), v3, itree(inode("newln", _), []),
      t4, itree(inode("=", _), []), v4, itree(inode("newln", _), [])
    ]), m) = 
      let
        val m1 = updateEnv(getLeaf t1, STRING, m)
        val m2 = updateStore(getLoc(accessEnv(getLeaf t1, m1)), symbol( getLeaf v1 ), m1)
        val m3 = updateEnv(getLeaf t2, STRING, m2)
        val m4 = updateStore(getLoc(accessEnv(getLeaf t2, m3)), symbol( getLeaf v2 ), m3)
        val m5 = updateEnv(getLeaf t3, STRING, m4)
        val m6 = updateStore(getLoc(accessEnv(getLeaf t3, m5)), symbol( getLeaf v3 ), m5)
        val m7 = updateEnv(getLeaf t4, STRING, m6)
        val m8 = updateStore(getLoc(accessEnv(getLeaf t4, m7)), symbol( getLeaf v4 ), m7)
      in
        m8
      end
      
  | M(itree(inode(x_root,_), children), _) = raise General.Fail("\n\nIn M root = " ^ x_root ^ "\n\n")
  | M _ = raise Fail("error in Semantics.M - this should never occur")


(*
  fun M(itree(inode("startInitialization", _),
    [start, itree(inode("=", _), []), value, itree(inode("newln", _), [])]), m) =
      let
        val m1 = updateEnv("start", STRING, m)
        val m2 = updateStore(getLoc(accessEnv("start", m1)), getLeaf value, m1)
      in
        m2
      end

  fun M(itree(inode("rwHeadInitialization", _),
    [rwHead, itree(inode("=", _), []), value, itree(inode("newln", _), []) ]), m) = 
      let
        val m1 = updateEnv("rw_head", STRING, m)
        val m2 = updateStore(getLoc(accessEnv("rw_head", m1)), getLeaf value, m1)
      in
        m2
      end
    
  fun M(itree(inode("whileLoop", nodeInfo),
    [whle, ruleFired, openBrace, expressions, closeBrace]), m) =
      let
        val v1 = accessStore(getLoc(accessEnv("rule_fired", m)), m)
      in
        if v1 = "true" then
          let
            val m1 = updateStore(getLoc(accessEnv("rule_fired", m)), "false", m)
            val m2 = M(expressions, m1)
          in
            M(itree(inode("whileLoop", nodeInfo), [whle, ruleFired, openBrace, expressions, closeBrace]), m2)
          end
        else
          m
      end

  fun M(itree(inode("expressions", _),
    [expression, itree(inode("\n", _), []), expressions]), m) = 
      M(expressions, M(expression, m))
  
  fun M(itree(inode("expression", _),
    [state, itree(inode("->", _), []), rule, itree(inode("->", _), []), newState]), m) =
      let
        val current = accessStore(getLoc(accessEnv("start", m)), m)
      in
        if current = getLeaf state then
          let
            val m1 = M(rule, m)
            val fired = accessStore(getLoc(accessEnv("rule_fired", m1)), m1)
          in
            if fired = "true" then updateStore(getLoc(accessEnv("start", m1)), getLeaf newState, m1) else m
          end
        else
          m
      end
    
  fun M(itree(inode("rule", _),
    [
      itree(inode("(", _), []), current, itree(inode(",", _), []), change, itree(inode(",", _), []), direction, itree(inode(")", _), [])
    ]), m) =
      let
        val tape = accessStore(getLoc(accessEnv("rw_head", m)), m)
        val value = accessStore(getLoc(accessEnv(tape, m)), m)
      in
        if getLeaf current = value then
          let
            val m1 = updateStore(getLoc(accessEnv(tape, m)), getLeaf change, m)
            val movement = case getLeaf direction of
              right => (
                case tape of
                  "T1" => "T2"
                | "T2" => "T3"
                | "T3" => "T4"
                | "T4" => raise Fail("cannot move read/write head past the end")
              ) | left => (
                case tape of
                  "T1" => raise Fail("cannot move read/write head before the start")
                | "T2" => "T1"
                | "T3" => "T2"
                | "T4" => "T3"
              )
            val m2 = updateStore(getLoc(accessEnv("rw_head", m1)), movement, m1)
            val m3 = updateStore(getLoc(accessEnv("rule_fired", m2)), "true", m2)
          in
            m3
          end
        else
          m
      end

*)


end