structure Interpreter = struct

  fun execute [programTree] =
    let
      (* The TLP representation of a tree is converted to a tree datatype similar to what we discussed in class *)
      val tree = Strategic_Values.getTerm programTree
      val result = Semantics.M(tree, Model.initialModel);
    in 
      (* you may want to output the final model in order to validate that there are no memory leaks in your code *)
      print("\n\n\n");
      print(" ======================================= \n");
      Model.printModel(result);
      print("\n ======================================= \n");
      print("\n\n\n")
    end
  | execute _ = raise Fail("Error in Interpreter.execute - this should never occur")

  val functions = [("interpreter_execute"  , Util.execVoid execute)]
  
end