structure Model = struct

  (* For your typeChecker you may want to have a datatype that defines the types 
  (i.e., integer, boolean and possibly error) in your language. *)
  datatype supported = BOOL | STRING

  (* It is recommended that your model store integers and Booleans in an internal form (i.e., as terms belonging to
   a userdefined datatype (e.g., denotable_value). If this is done, the store can be modeled as a list of such values.
  *)
  datatype value = boolean of string | symbol of string

  type addr  = int
  type env   = (string * supported * addr) list
  type store = (addr * value) list

  val initialModel = ([]:env, 0:addr, []:store)

  (* This function may be useful to get the leaf node of a tree -- which is always a string (even for integers).
   It is up to the interpreter to translate values accordingly (e.g., string to integer and string to boolean).
     
   Consult (i.e., open Int and open Bool) the SML structures Int and Bool for functions that can help with 
   this translation. 
  *)
  fun getLeaf(term) = CONCRETE.leavesToStringRaw term
  
  fun accessEnv(id, ((entryId, dataType, addr) :: rest, _, _)) =
    if id = entryId then (dataType, addr) else accessEnv(id, (rest, 0, []))
  | accessEnv(id, ([], _, _)) = raise General.Fail("Error: var " ^ id ^ " has not been declared")
    
  fun updateEnv(id, dataType, (env, addr, store)) = ((id, dataType, addr) :: env, addr + 1, store)
    
  fun accessStore(location, (_, _, (addr, value) :: rest)) =
    if location = addr then value else accessStore(location, ([], 0, rest))
    | accessStore(location, (_, _, [])) = raise General.Fail("Error: var for memory location " ^ location ^ " has not been set")
    
  fun updateStore(location, value, (env, addr, store)) = (env, addr, (location, value) :: store)
  
  fun getType(dataType, location) = dataType
  fun getLoc(dataType, location) = location
  
  fun printModel((env, addr, (loc, boolean value) :: rest)) = print(Int.toString loc ^ " = " ^ value)
    | printModel((env, addr, (loc, symbol  value) :: rest)) = print(Int.toString loc ^ " = " ^ value)
    | printModel((env, addr, [])) = print("")

end











