(* ============================================================================================== *) 
datatype lexresult  = SHELL of string * string * {line: word, column: word};
val error           = fn x => TextIO.output(TextIO.stdOut,x ^ "\n")
val eof             = fn () => SHELL("","eof",getNextTokenPos(""))
val counter         = ref 0;
(* ============================================================================================== *)
(* ------------------------------------------------------------------ *)
(* assumes that ">" does not occur as part of a nonterminal symbol *)
fun generateSchemaTokenName( yytext ) =
    let
        fun split(x, []   ) =  raise General.Fail("an_error")
          | split(x, y::ys) = if x=y then ys else split(x,ys);
                                                    
        fun splitFirst(symbol,[])    =     [] (* symbol was not in the input list *)
          | splitFirst(symbol,x::xs) =     if x = symbol 
                        then (* found split point *)
                            []
                        else (* keep looking      *)
                            x::splitFirst(symbol,xs);
                                                                        
        val s0   = explode(yytext);
        val s1   = split(#"<",s0);
        val s2   = splitFirst(#">",s1);  
    in
        implode(explode("!#schema_variable_") @ s2)        
    end;
    
(* ------------------------------------------------------------------ *)

(* ============================================================================================== *)
%%
%header (functor Target_LexFn(val getNextTokenPos : string -> {line: word, column: word}));


digit        = [0-9];
alpha        = [A-Za-z];
alphanumeric = [A-Za-z0-9_];

langAlpha    = [b0-9];
integer      = {digit} + (\.{digit}+)?;
stateIdent   = "S" {integer};

ws           = [\  \t];
alphanumeric = [A-Za-z0-9_];
schema       = "<" {alpha}{alphanumeric}* ">_" {alphanumeric}+;

%%

<INITIAL> "T1"                      => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "T2"                      => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "T3"                      => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "T4"                      => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "start"                   => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "rw_head"                 => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "left"                    => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "right"                   => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "while"                   => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "rule_fired"              => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );

<INITIAL> "\n"                      => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "="                       => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "->"                      => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "("                       => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> ")"                       => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "{"                       => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> "}"                       => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );
<INITIAL> ","                       => ( SHELL(yytext                            , yytext,     getNextTokenPos(yytext))    );

<INITIAL> {langAlpha}               => ( SHELL("alphabet"                        , yytext,     getNextTokenPos(yytext))    );
<INITIAL> {stateIdent}+             => ( SHELL("state"                           , yytext,     getNextTokenPos(yytext))    );

<INITIAL> {ws}+                     => ( getNextTokenPos(yytext); lex()  );
<INITIAL> {schema}                  => ( SHELL(generateSchemaTokenName(yytext)   , yytext,     getNextTokenPos(yytext))    );
<INITIAL> .                         => ( error("ignored an unprintable character: " ^ yytext); getNextTokenPos(yytext); lex()  );
